# GNU Radio Blocks for Raptor

### Table of Contents
**[Introduction](#introduction)**<br>
**[Resources](#resources)**<br>
**[Installation](#installation)**<br>
**[Licensing](#licensing)**<br>

## Introduction
Raptor is a next-generation Software-Defined Radio (SDR) development board that combines the processing
performance of the Xilinx Zynq UltraScale+ MPSoC with the flexibility of Analog Device’s AD9361 RF Agile
Transceiver. While maintaining a small and compact form-factor, Raptor includes key signal conditioning features
necessary to implement a complete and robust radio solution capable of operation over a 70 MHz to 6
GHz frequency range.

An expansion header, located on the back side of the board, provides access to several additional digital I/O
interfaces using an optional mezzanine board. A mezzanine which adds 10/100/1000Base-T Ethernet, two SFP+ cages,
mini DisplayPort, M.2 SATA, Samtec FireFly, and a 16-bit general-purpose I/O header is currently available for
Raptor.

GNU Radio + Raptor allows users to quickly develop powerful DSP solutions
for a wide variety of applications. GNU Radio can run directly directly on Raptor, or it can be run on a separate host machine, streaming raw RF data directly from the raptor via USB or Network connection. For "GNU Radio + Raptor" how-to information, please visit the [Usage](https://gitlab.com/rinconresearch/gr-raptor/wikis/Usage/Blocks) section of our wiki.

<br>
![Raptor](https://gitlab.com/rinconresearch/gr-raptor/uploads/57fb6db22120d32631abccb4316c1f13/spectrum_analyze.png)
<br>

## Resources

*Here are some links you may find useful.*

* Gr-Raptor [Wiki](https://gitlab.com/rinconresearch/gr-raptor/wikis/home)
* Raptor [Product Brief](https://www.rincon.com/wp-content/uploads/2018/03/Raptor_Brief_v1-5.pdf)
* Raptor [User Guide](http://raptor.rincon.com)
* Libiio [API Reference](http://analogdevicesinc.github.io/libiio/)
* GNU Radio [Tutorials](https://wiki.gnuradio.org/index.php/Guided_Tutorials)

## Installation
There are two ways you can install the Raptor OOT Module. The first (and probably easiest)
is with pybombs. Alternatively you can build from source.

*Note: All dependencies will come pre-installed on your raptor sd card. In addition, If installing using PyBombs method, dependencies will be automatically installed.*

#### Using PyBombs
Instructions for getting started with PyBombs and configuring your prefix can be found [here](https://www.gnuradio.org/blog/pybombs-the-what-the-how-and-the-why). After PyBombs is setup, add the gr-raptor recipe and then install.

    pybombs recipes add gr-raptor https://gitlab.com/rinconresearch/gr-raptor.git
    pybombs install gr-raptor

If your installation completes successfully, you're done! Source the prefix you installed gr-raptor under and start up GNU Radio. If you have any issues, please refer to the pybombs documentation.

#### Building from source
**Install Dependencies**

    sudo apt-get -y install gnuradio-dev libxml2 libxml2-dev bison flex cmake git libaio-dev libboost-all-dev libcdk5-dev libusb-1.0-0-dev libserialport-dev libavahi-client-dev doxygen graphviz

<space>

    git clone https://github.com/analogdevicesinc/libad9361-iio.git
    cd libad9361
    cmake .
    make
    sudo make install

<space>

    git clone https://github.com/analogdevicesinc/libiio.git
    cd libiio
    cmake .
    make all
    sudo make install

**Build and Install GNU Radio + Raptor**

    git clone https://gitlab.com/rinconresearch/gr-raptor/gr-raptor.git
    cd gr-raptor
    cmake .
    make -j2
    sudo make install

If you have multiple GNU Radio prefixes, use this cmake flag to specify the desired path.

    cmake -DCMAKE_INSTALL_PREFIX=<gnuradio install prefix path> .

## License
**This project is licensed under the [GNU General Public License v3.0](https://gitlab.com/rinconresearch/gr-raptor/blob/master/LICENSE)**
