# Copyright 2011,2012 Free Software Foundation, Inc.
#
# This file is part of GNU Radio
#
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

########################################################################
# Setup library
########################################################################
include(GrPlatform) #define LIB_SUFFIX

include_directories(${Boost_INCLUDE_DIR} ${RAPTOR_INCLUDE_DIRS} ${AD9361_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

include(FindFLEX REQUIRED)
find_package(BISON "3.0.2" REQUIRED)

list(APPEND raptor_sources
        device_source_impl.cc
        device_sink_impl.cc
        raptor_source_impl.cc
        raptor_sink_impl.cc
        ${FLEX_lexer_OUTPUTS}
        ${BISON_parser_OUTPUTS}
        )

set(raptor_sources "${raptor_sources}" PARENT_SCOPE)
if (NOT raptor_sources)
    MESSAGE(STATUS "No C++ sources... skipping lib/")
    return()
endif (NOT raptor_sources)

if ("${PC_GNURADIO_RUNTIME_VERSION}" VERSION_LESS 3.8.0)
    set(GR_IS_VERSION_3_7_OR_LESS ON)
endif ()

option(COMPAT_GR_3_7_OR_LESS "Compile for GNU Radio version 3.7 or less" ${GR_IS_VERSION_3_7_OR_LESS})
if (COMPAT_GR_3_7_OR_LESS)
    add_definitions(-DGR_VERSION_3_7_OR_LESS)
endif ()

add_library(gnuradio-raptor SHARED ${raptor_sources})
target_link_libraries(gnuradio-raptor
        ${Boost_LIBRARIES}
        ${RAPTOR_LIBRARIES}
        ${AD9361_LIBRARIES}
        ${GNURADIO_ALL_LIBRARIES})
set_target_properties(gnuradio-raptor PROPERTIES
        VERSION ${GR_RAPTOR_VERSION}
        SOVERSION ${GR_RAPTOR_VERSION_MAJOR}
        DEFINE_SYMBOL "gnuradio_raptor_EXPORTS"
        )

if (APPLE)
    set_target_properties(gnuradio-raptor PROPERTIES
            INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib"
            )
endif (APPLE)

########################################################################
# Install built library files
########################################################################
install(TARGETS gnuradio-raptor
        LIBRARY DESTINATION ${GR_LIBRARY_DIR} # .so/.dylib file
        ARCHIVE DESTINATION ${GR_LIBRARY_DIR} # .lib file
        RUNTIME DESTINATION ${GR_RUNTIME_DIR} # .dll file
        )


########################################################################
# Setup the include and linker paths
########################################################################
#include_directories(
#	${GR_RAPTOR_INCLUDE_DIRS}
#   ${GNURADIO_RUNTIME_INCLUDE_DIRS}
#   ${Boost_INCLUDE_DIRS}
