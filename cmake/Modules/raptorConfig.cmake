INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(RAPTOR raptor)

FIND_PATH(
    RAPTOR_INCLUDE_DIRS
    NAMES raptor/api.h
    HINTS $ENV{RAPTOR_DIR}/include
        ${PC_RAPTOR_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    RAPTOR_LIBRARIES
    NAMES gnuradio-raptor
    HINTS $ENV{RAPTOR_DIR}/lib
        ${PC_RAPTOR_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(RAPTOR DEFAULT_MSG RAPTOR_LIBRARIES RAPTOR_INCLUDE_DIRS)
MARK_AS_ADVANCED(RAPTOR_LIBRARIES RAPTOR_INCLUDE_DIRS)
