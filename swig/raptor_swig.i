/*
 * Copyright 2018 Rincon Research Corporation
 * Author(s): Daniel Copley, Paul Cercueil
 * Code forked from Analog Devices Inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#define RAPTOR_API

%include "gnuradio.i"

%{
#include "raptor/device_source.h"
#include "raptor/device_sink.h"
#include "raptor/raptor_source.h"
#include "raptor/raptor_sink.h"
%}

%include "raptor/device_source.h"
%include "raptor/device_sink.h"
%include "raptor/raptor_source.h"
%include "raptor/raptor_sink.h"

GR_SWIG_BLOCK_MAGIC2(raptor, device_source);
GR_SWIG_BLOCK_MAGIC2(raptor, device_sink);
GR_SWIG_BLOCK_MAGIC2(raptor, raptor_source);
GR_SWIG_BLOCK_MAGIC2(raptor, raptor_source_f32c);
GR_SWIG_BLOCK_MAGIC2(raptor, raptor_sink);
GR_SWIG_BLOCK_MAGIC2(raptor, raptor_sink_f32c);
